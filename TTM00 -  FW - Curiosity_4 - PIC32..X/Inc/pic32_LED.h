
/* 
 * File:   pic32_LED.h
 * Author: nb1
 *
 * Created on August 7, 2019, 1:06 AM
 */

#include<stdint.h>
#include <plib.h>

enum
{
    e_LED_OFF,
    e_LED_ON
};

enum
{
    e_LED_1,
    e_LED_2,
    e_LED_3,   
    e_LED_green,
    e_LED_blue,
    e_LED_red
    
};

typedef struct
  {
    uint8_t LED_NAME;
    uint32_t LED_pin; 
    uint8_t LED_port;  
    uint8_t LED_status;  
  }led_struct_t;

extern led_struct_t LED1_struct;
extern led_struct_t LED2_struct;
extern led_struct_t LED3_struct;
extern led_struct_t LED_green_struct;
extern led_struct_t LED_blue_struct;
extern led_struct_t LED_red_struct;

void LED_on(uint8_t LED);
void LED_off(uint8_t LED);
void LED_toggle(uint8_t LED);








