/* 
 * File:   main.h
 * Author: nb1
 *
 * Created on August 29, 2019, 3:16 PM
 */

#include"general_configure.h"
#include"pic32_uart.h"
#include<stdio.h>
#include <string.h>
#include<stdlib.h>
#include<xc.h>
#include<stdio.h>
#include<stdint.h>
#include"plib.h"
#include "pic32_LED.h"


#define FOSC 96E6
// TICK_PERIOD_CORE_TIMER can never be larger than 2^32, because of uint_32 containers
#define TICK_PERIOD_CORE_TIMER (FOSC/2000)
// TICK_PERIOD_TIMER1 can never be larger than 2^16, because of uint_16 containers
#define TICK_PERIOD_TIMER1 (FOSC/8000)

//parameters for CN  
#define CONFIG (CND_ON | CND_IDLE_CON)
#define PINS (CND6_ENABLE)
#define PULLUPS (CND_PULLUP_DISABLE_ALL)
#define INTERRUPT (CHANGE_INT_ON | CHANGE_INT_PRI_2)

// define setup parameters for OpenADC10
#define PARAM1 ADC_MODULE_ON | ADC_FORMAT_INTG | ADC_CLK_AUTO |ADC_AUTO_SAMPLING_ON
#define PARAM2 ADC_VREF_AVDD_AVSS | ADC_OFFSET_CAL_DISABLE | ADC_SCAN_OFF| ADC_SAMPLES_PER_INT_2 | ADC_ALT_BUF_ON | ADC_ALT_INPUT_ON
#define PARAM3 ADC_CONV_CLK_INTERNAL_RC | ADC_SAMPLE_TIME_12
#define PARAM4 SKIP_SCAN_ALL
#define PARAM5 ENABLE_AN14_ANA 

// definitions for giving initials values to pwms since the program
//ms total time on
//LED_pwm_on_init can never be larger than 2^16, because of uint_16 containers
#define LED_pwm_on_init 1
//ms total time (period)
//LED_pwm_init can never be larger than 2^16, because of uint_16 containers
#define LED_pwm_init 20
//macro time on
// pwm_on_init can never be larger than 2^16, because of uint_16 containers
#define pwm_on_init 1000
//macro time on
//pwm_init can never be larger than 2^16, because of uint_16 containers
#define pwm_init 1500

// define macros for the representative's values of the boards.
#define UART_TO_COMPUTER 1
#define UART_TO_BOARD 2



//unions for program

union uint32_and_uint8{
    uint32_t uint_32_bit_variable;
    uint8_t uint_8_bit_array[4];
}element;

//variables for program
typedef volatile struct
{
  uint16_t time_S1_pressed_for_toggle_led_1; 
  uint16_t time_S1_pressed_for_enable_blinking_led2;
  uint16_t S1_changes;
  uint16_t time_for_tick;
  uint8_t *pointer_UART;
  uint8_t message[64];
  uint16_t character_of_comand;
  uint8_t comand[64];
  uint16_t data_numeric;
  uint16_t chosen_board;
  uint16_t comand_determinated;
  uint8_t characters_of_numbers[4];
  uint8_t comand_valid :1;
  uint8_t comand_dont_exist :1;
          
}variables_program_t;

//variables for pwm
typedef volatile struct
{
  uint16_t LED_pwm_on;
  uint16_t LED_pwm;
  uint16_t pwm_on;
  uint16_t  pwm;
  uint16_t LED_pwm_on_init_modificated;
  uint16_t pwm_on_init_modificated;
  uint16_t pwm_init_modificated;
//  uint16_t LED_millis;
  uint16_t LED_pwm_init_modificated;
}pwm_variables_t;

//variables for blinking in Led2
typedef volatile struct
{
 //state of counter led1
 uint8_t state_counter;   
 //state of counter led2
 uint8_t state_counter_led2;
 //state of blinking
 uint8_t state_of_blinking;
 //counter for led2
 uint16_t blinking_time;
}counters_led_t;


enum{
  e_count_off,
  e_count_on,
  e_time_passed_500mils,   
  e_time_passed_2000mils,  
  e_blinking_led2_on,   
  e_blinking_led2_off,
  e_modificate_micro_time_on_led_green,
  e_modificate_micro_time_led_green,
  e_modificate_macro_time_on_led_green,
  e_modificate_macro_time_led_green,
  e_modificate_micro_time_on_led_blue,
  e_modificate_micro_time_led_blue,
  e_modificate_macro_time_on_led_blue,
  e_modificate_macro_time_led_blue,
  e_get_ADC_value,
  e_disable_led_green,
  e_enable_led_green,
  e_disable_led_blue,
  e_enable_led_blue
};

//Function Prototypes
void print_times_for_pwm_led_green(void);
void print_times_for_pwm_led_blue(void);
void init_UART(void);
void initialize_timers(void);
void init_variables(void);
void initialize_peripherals(void);
void initialize_adc(void);
void receive_UART2(void);
void send_tick_via_uart(void);
void check_SW1(void);
void control_blinkid_led_2(void);
void control_toggle_led1(void);
void control_pwm(void);
void determinate_brigtnes_pwm(void);
void control_LED_red_with_CN_interrupt(void);
void control_pwm_green(void);
void control_pwm_blue(void);
void receive_instruction_of_UART1(void);
void execute_comand(void);
void verify_comand(void);
void print_message_for_appropriate_uart(uint8_t *string);