#include"main.h"

//variables for the program
variables_program_t variables;
uint16_t vu16_LED_macro=0;
pwm_variables_t pwm_led3;
pwm_variables_t pwm_green;
pwm_variables_t pwm_blue;
counters_led_t parameters_led2;



void main(void) 
{
    init_UART();
    initialize_timers();    
    init_variables();
    initialize_adc();
    initialize_peripherals();
  

    while(1)
    {
        receive_UART2() ;
        send_tick_via_uart();
        check_SW1();
        control_blinkid_led_2();
        control_toggle_led1();
        control_pwm();
        determinate_brigtnes_pwm();
        control_LED_red_with_CN_interrupt();
        receive_instruction_of_UART1();
        if(LED_green_struct.LED_status==e_LED_ON)
        control_pwm_green();
        if(LED_blue_struct.LED_status==e_LED_ON)
        control_pwm_blue(); 
    };
}

//For Timer1Handler function:
/**
    * @brief    decrease counters corresponding to base of time for UART message
    */
void __ISR(_TIMER_1_VECTOR, ipl3SOFT) Timer1Handler(void)
{
    // clear the interrupt flag
    mT1ClearIntFlag();
    //timer for UART message
    if(variables.time_for_tick) 
        variables.time_for_tick--;
    //decrease counters
    if(pwm_led3.LED_pwm_on)
        pwm_led3.LED_pwm_on--;
    if(pwm_led3.LED_pwm) 
        pwm_led3.LED_pwm--;
    if(pwm_led3.pwm_on) 
        pwm_led3.pwm_on--;
    if(pwm_led3.pwm) 
        pwm_led3.pwm--;
    //decrease counters of pwm led green
    if( pwm_green.LED_pwm_on)
        pwm_green.LED_pwm_on--;
    if(pwm_green.LED_pwm) 
        pwm_green.LED_pwm--;
    if(pwm_green.pwm_on) 
        pwm_green.pwm_on--;
    if(pwm_green.pwm) 
        pwm_green.pwm--;
}

//For _CoreTimerHandler function:
/**
    * @brief    decrease counters corresponding to base of time of SW1.
    */
void __ISR(_CORE_TIMER_VECTOR, ipl2SOFT) _CoreTimerHandler(void)
{
    // clear the interrupt flag
    mCTClearIntFlag();
    // update the period
    UpdateCoreTimer(TICK_PERIOD_CORE_TIMER);
    //decrease counters when are activated
   if(variables.time_S1_pressed_for_toggle_led_1) 
       variables.time_S1_pressed_for_toggle_led_1--;

   if(variables.time_S1_pressed_for_enable_blinking_led2) 
       variables.time_S1_pressed_for_enable_blinking_led2--;
    if(parameters_led2.state_of_blinking==e_blinking_led2_on)
   {
        if(parameters_led2.blinking_time)
            parameters_led2.blinking_time--;
    }
    if( pwm_blue.LED_pwm_on)
        pwm_blue.LED_pwm_on--;
    if(pwm_blue.LED_pwm) 
        pwm_blue.LED_pwm--;
    if(pwm_blue.pwm_on) 
        pwm_blue.pwm_on--;
    if(pwm_blue.pwm) 
        pwm_blue.pwm--;
} 

//For ChangeNotice_Handler function:
/**
    * @brief    decrease counters for toggle led red.
    */

void __ISR(_CHANGE_NOTICE_VECTOR, ipl2SOFT) ChangeNotice_Handler(void)
{
 // clear the mismatch condition
 mPORTDRead();

 // clear the interrupt flag
 mCNDClearIntFlag();
 //count changes in the pin
 if(variables.S1_changes)
  variables.S1_changes--;
 
}

//For init_UART function:
/**
    * @brief     init UART for send datta   
    */
void init_UART(void)
{
    UART1_init(115200);
//    UART1_write_string("Pint to console... \n");
    UART2_init(115200);
    UART2_write_string("Pint to console... \n");
}

//For initialize_timers function:
/**
    * @brief    initialize the timers
    */
void initialize_timers(void)
{

  //configure the timers
 OpenCoreTimer(TICK_PERIOD_CORE_TIMER);
 OpenTimer1(T1_ON | T1_SOURCE_INT | T1_PS_1_8,TICK_PERIOD_TIMER1);
 mCNDOpen(CONFIG, PINS, PULLUPS);

 //set timers interrupt level
 mConfigIntCoreTimer(CT_INT_ON | CT_INT_PRIOR_2);
 ConfigIntTimer1(T1_INT_ON | T1_INT_PRIOR_1);
 ConfigIntCND(INTERRUPT);

 //enable multi-vector interrupts
 INTEnableSystemMultiVectoredInt();
  
}

//For init_variables function:
/**
    * @brief     initialization of variables   
    */
void init_variables(void)
 {
   element.uint_8_bit_array[0] = 0xDD;
   element.uint_8_bit_array[1] = 0xCC;
   element.uint_8_bit_array[2] = 0xBB;
   element.uint_8_bit_array[3] = 0xAA;
   PRINT_MESSAGE("[PIC] element: %X\n", element.uint_8_bit_array[0]);
   
   //counters
   variables.time_for_tick=1500;
   variables.time_S1_pressed_for_toggle_led_1=500; 
   variables.time_S1_pressed_for_enable_blinking_led2=2000;
   parameters_led2.blinking_time=1000; 
   variables.S1_changes=2;
   parameters_led2.state_counter= e_count_off;
   parameters_led2.state_counter_led2=e_count_off;
   parameters_led2.state_of_blinking= e_blinking_led2_on; 
   
    LED1_struct.LED_NAME=e_LED_1;
    LED1_struct.LED_port='E';
    LED1_struct.LED_pin = BIT_4;
    LED1_struct.LED_status = e_LED_OFF;
    
    LED2_struct.LED_NAME=e_LED_2;
    LED2_struct.LED_port = 'E';
    LED2_struct.LED_pin = BIT_6;
    LED2_struct.LED_status = e_LED_OFF;
    
    LED3_struct.LED_NAME=e_LED_3;
    LED3_struct.LED_port = 'E';
    LED3_struct.LED_pin = BIT_7;
    LED3_struct.LED_status = e_LED_OFF;
    
    LED_green_struct.LED_NAME=e_LED_green;
    LED_green_struct.LED_port='B';
    LED_green_struct.LED_pin=BIT_3;
    LED_green_struct.LED_status= e_LED_ON;
    
    LED_blue_struct.LED_NAME=e_LED_blue;
    LED_blue_struct.LED_port='B';
    LED_blue_struct.LED_pin=BIT_2;
    LED_blue_struct.LED_status=e_LED_ON;
    
    LED_red_struct.LED_NAME=e_LED_red;
    LED_red_struct.LED_port='B';
    LED_red_struct.LED_pin= BIT_10;
    LED_red_struct.LED_status=e_LED_ON;
    
    pwm_led3.LED_pwm_on=LED_pwm_on_init;
    pwm_led3.LED_pwm=LED_pwm_init;
    pwm_led3.pwm_on=pwm_on_init;
    pwm_led3.pwm=pwm_init;
    pwm_led3.LED_pwm_on_init_modificated=LED_pwm_on_init;
    
    pwm_green.LED_pwm_on=LED_pwm_on_init;
    pwm_green.LED_pwm=LED_pwm_init;
    pwm_green.pwm_on=pwm_on_init;
    pwm_green.pwm=pwm_init;
    pwm_green.LED_pwm_on_init_modificated=LED_pwm_on_init;
    pwm_green.pwm_on_init_modificated=pwm_on_init;
    pwm_green.pwm_init_modificated=pwm_init;
    pwm_green.LED_pwm_init_modificated=LED_pwm_init;
    
    pwm_blue.LED_pwm_on=LED_pwm_on_init;
    pwm_blue.LED_pwm=LED_pwm_init;
    pwm_blue.pwm_on=pwm_on_init;
    pwm_blue.pwm=pwm_init;
    pwm_blue.LED_pwm_on_init_modificated=LED_pwm_on_init;
    pwm_blue.pwm_on_init_modificated=pwm_on_init;
    pwm_blue.pwm_init_modificated=pwm_init;
    pwm_blue.LED_pwm_init_modificated=LED_pwm_init;
 }

//For initialize_peripherals function:
/**
    * @brief    initialize the peripherials
    */

void initialize_peripherals(void)

{
   //Initializing leds 1,2,3
    mPORTESetPinsDigitalOut(LED1_struct.LED_pin);
    mPORTESetPinsDigitalOut(LED2_struct.LED_pin);
    mPORTESetPinsDigitalOut(LED3_struct.LED_pin);
   //Initializing Switch1
    mPORTDSetPinsDigitalIn(BIT_6);
  //Configuration pins for leds green, blue and red 
   mPORTBSetPinsDigitalOut(BIT_3);
   mPORTBSetPinsDigitalOut(BIT_10);
   mPORTBSetPinsDigitalOut(BIT_2);
   //Initializing led green, blue and red 
   LED_on(LED_red_struct.LED_NAME);
   LED_off(LED_green_struct.LED_NAME);
   LED_off(LED_blue_struct.LED_NAME);
          
   //PERIPHERALS FOR UART2
  ANSELBbits.ANSB9=0;
  ANSELEbits.ANSE5=0; 
   
    LED_on(LED1_struct.LED_NAME);
    LED_on(LED3_struct.LED_NAME);
    
     
    //read port(s) to clear mismatch on change notice pins
    mPORTDRead();
}

//For initialize_adc function:
/**
    * @brief    initialize the adc
    */
void initialize_adc(void)
{
 // configure and enable the ADC
 CloseADC10();// ensure the ADC is off before setting the configuration
  
  // configure to sample AN14 
 SetChanADC10( ADC_CH0_NEG_SAMPLEA_NVREF | ADC_CH0_POS_SAMPLEA_AN14); 

  // configure ADC and enable it
 OpenADC10( PARAM1, PARAM2, PARAM3, PARAM4, PARAM5 ); 
}

//For receive_UART2 function:
/**
    * @brief     receive UART2 message and realize the respective action   
    */
void receive_UART2(void)
{
    if (UART2_is_unread())
   {
        UART2_write_string("RECEIVED. \n\r");
        if((U2RxBuf[0]<48) || (U2RxBuf[0]>57))
        {
            UART1_write_string(U2RxBuf);
        }
        else
        {    
            parsing_to_message();
            if (variables.chosen_board==UART_TO_COMPUTER)
           {      
               verify_comand();
               execute_comand();
            }
            if (variables.chosen_board==UART_TO_BOARD)
           {
                UART1_write_string(U2RxBuf);
            }
        }  
        memset(U2RxBuf,0,U2RxBufSize);
        UART2_clear_unread(); 
    }
}

//For send_uart_message_if_passed_vu16_UART_time function:
/**
    * @brief    send a message for uart every 1.5 seconds.
    */

void send_tick_via_uart(void)
{
  //for UART message 
 if (!variables.time_for_tick)
 {
   variables.time_for_tick=1500;
//    UART1_write_string("TICK1\n\r");
    UART2_write_string("TICK2\n\r");
    
  }  
}

//For check_SW1 function:
/**
    * @brief    check sw1 and decide actions according to the time that is pressed.
    */

void check_SW1(void)
{
    //for switch1
    if(PORTDbits.RD6==0)
    {
        //for led1 toggle and led2 blinking on or off
        if(parameters_led2.state_counter==e_count_off)
        {
            parameters_led2.state_counter=e_count_on;
            variables.time_S1_pressed_for_toggle_led_1 = 500;
            variables.time_S1_pressed_for_enable_blinking_led2 = 2000;
        }
    }
        
    else
    {
        parameters_led2.state_counter=e_count_off;

    }
}

//For control_blinkid_led_2 function:
/**
    * @brief    control the turn on or turn off of led 2 according to the time.
    */
void control_blinkid_led_2(void)

{
    //for led2 blinking on or off
    if ((!variables.time_S1_pressed_for_enable_blinking_led2)&& (parameters_led2.state_counter==e_time_passed_500mils))
   {
       if(parameters_led2.state_of_blinking==e_blinking_led2_on)
       {
            parameters_led2.state_of_blinking=e_blinking_led2_off;
        }
        else
       {
            parameters_led2.state_of_blinking=e_blinking_led2_on;
        }
        parameters_led2.state_counter=e_time_passed_2000mils;
    }    
    //for led 2 every second
    if (!parameters_led2.blinking_time)
    {
        parameters_led2.blinking_time = 1000;
        LED_toggle( LED2_struct.LED_NAME);
    }
}

//For control_toggle_led1 function:
/**
    * @brief    control the turn on or turn off of led 1 according to the time that sw1 is pressed.
    */

void control_toggle_led1(void)
{
  //for led1 toggle
   if ((!variables.time_S1_pressed_for_toggle_led_1)&&(parameters_led2.state_counter==e_count_on))
    {
     LED_toggle( LED1_struct.LED_NAME);
     parameters_led2.state_counter=e_time_passed_500mils;
    }
}

//For control_pwm function:
/**
    * @brief    control the turn on or turn off of led 3 according to the times for pwm functionality.
    */

void control_pwm(void)
{
  //for pwm
 if(pwm_led3.pwm_on)
 {
    if( pwm_led3.LED_pwm_on)
    {
      LED_on( LED3_struct.LED_NAME);
    }
    else
    {
       LED_off( LED3_struct.LED_NAME);
    }    
    if(!pwm_led3.LED_pwm)
    {
       pwm_led3.LED_pwm=LED_pwm_init;
       pwm_led3.LED_pwm_on=pwm_led3.LED_pwm_on_init_modificated;
    }
 }
 else
 { 
   LED_off( LED3_struct.LED_NAME);
 }
 if(!pwm_led3.pwm)
 {
   pwm_led3.pwm=pwm_init;
   pwm_led3.pwm_on=pwm_on_init;
 }
}

//For determinate_brigtnes_pwm function:
/**
    * @brief    Read adc and determinate brightness
    */
void determinate_brigtnes_pwm(void)
{
// conversion result as read from result buffer
unsigned int value_adc_an14;
// points to the base of the idle buffer
unsigned int offset; 
//adc_valu_in_porcentage can never be larger than 2^16, because of uint_16 containers
uint16_t adc_value_in_porcentage;
  offset = 8 * ((~ReadActiveBufferADC10() & 0x01));
 // read the result of channel 4 conversion in the idle buffer
  value_adc_an14 = ReadADC10(offset);
  //calculate time value for pwm in led 3
  adc_value_in_porcentage=value_adc_an14*100/1023;
  pwm_led3.LED_pwm_on_init_modificated=adc_value_in_porcentage*LED_pwm_init/100 ;
}

//For control_LED_green_with_CN_interrupt function:
/**
    * @brief    do toggle to the green led according to the CN interrupt
    */

void control_LED_red_with_CN_interrupt(void)
{
    // do toggle to the led green
    if(!variables.S1_changes)
    {
          //mPORTBToggleBits(BIT_10); 
        LED_toggle( LED_red_struct.LED_NAME);  
        variables.S1_changes=2;
    } 
}

//For control_pwm_green function:
/**
    * @brief    control the turn on or turn off of led green according to the times from UART.
    */

void control_pwm_green(void)
{
    //for pwm led green
    if(pwm_green.pwm_on)
    {
        if( pwm_green.LED_pwm_on)
        {
            LED_on(LED_green_struct.LED_NAME);
        }
        else
        {
            LED_off(LED_green_struct.LED_NAME);
        }  
        if(!pwm_green.LED_pwm)
        {
            pwm_green.LED_pwm=pwm_green.LED_pwm_init_modificated;
            pwm_green.LED_pwm_on=pwm_green.LED_pwm_on_init_modificated;
        }
    }
    else
    { 
        LED_off(LED_green_struct.LED_NAME);
    }
    if(!pwm_green.pwm)
    {
        pwm_green.pwm=pwm_green.pwm_init_modificated;
        pwm_green.pwm_on=pwm_green.pwm_on_init_modificated;
    }
}

//For control_pwm_blue function:
/**
    * @brief    control the turn on or turn off of led blue according to the times from UART.
    */

void control_pwm_blue(void)
{
    //for pwm led blue
    if(pwm_blue.pwm_on)
    {
        if( pwm_blue.LED_pwm_on)
        {
            LED_on(LED_blue_struct.LED_NAME);
        }
        else
        {
             LED_off(LED_blue_struct.LED_NAME);
        }  
        if(!pwm_blue.LED_pwm)
        {
            pwm_blue.LED_pwm=pwm_blue.LED_pwm_init_modificated;
            pwm_blue.LED_pwm_on=pwm_blue.LED_pwm_on_init_modificated;
        }
    }
    else
    { 
        LED_off(LED_blue_struct.LED_NAME);
    }
    if(!pwm_blue.pwm)
    {
        pwm_blue.pwm=pwm_blue.pwm_init_modificated;
        pwm_blue.pwm_on=pwm_blue.pwm_on_init_modificated;
    }
}

//For receive_instruction_of_UART1 function:
/**
    * @brief    receive and process the instruction received with the UART1
    */

void receive_instruction_of_UART1(void)
 {
   if (UART1_is_unread())
   {
        UART2_write_string("Received!\n\r");  
        if((U1RxBuf[0]<48) || (U1RxBuf[0]>57))
        {
           UART2_write_string(U1RxBuf); 
        }        
        else
        {    
           parsing_to_message();
           verify_comand();
           execute_comand();
        }
        UART1_clear_unread();
        memset(U1RxBuf,0,U1RxBufSize);
    }  
  }

//For send_ADC_value function:
/**
    * @brief    send for UART the ADC value
    */
void send_ADC_value(void)
{
   // conversion result as read from result buffer
unsigned int value_adc_an14;
// points to the base of the idle buffer
unsigned int offset; 
  // determine which buffer is idle and create an offset
    offset = 8 * ((~ReadActiveBufferADC10() & 0x01));
    // read the result of channel 4 conversion in the idle buffer
    value_adc_an14 = ReadADC10(offset);
    // UART1_write_string(channel4);
    uint8_t  UART1_TX_buffer[100];
    sprintf(UART1_TX_buffer, "ADC: %d\n\r", value_adc_an14);
    switch(variables.chosen_board)
    { 
        case 1:
            UART2_write_string(UART1_TX_buffer);
        break;
        case 2:
            UART1_write_string(UART1_TX_buffer);
        break;    
    }
}

//For initializate_pwm_green function:
/**
    * @brief    sincronize times of pwm led green
    */

void initializate_pwm_green(void)
{
pwm_green.LED_pwm_on=pwm_green.LED_pwm_on_init_modificated;
pwm_green.LED_pwm=pwm_green.LED_pwm_init_modificated;
pwm_green.pwm_on=pwm_green.pwm_on_init_modificated;
pwm_green.pwm=pwm_green.pwm_init_modificated;
}

//For initializate_blue_green function:
/**
    * @brief    sincronize times of pwm led blue
    */
void initializate_pwm_blue(void)
 {
   pwm_blue.LED_pwm_on=pwm_blue.LED_pwm_on_init_modificated;
   pwm_blue.LED_pwm=pwm_blue.LED_pwm_init_modificated;
   pwm_blue.pwm_on=pwm_blue.pwm_on_init_modificated;
   pwm_blue.pwm=pwm_blue.pwm_init_modificated;
  }

//For execute_comand function:
/**
    * @brief    realize actions according to the instruction received
    */
void execute_comand(void)
 {
   switch(variables.comand_determinated)
   {
       case e_get_ADC_value:
            send_ADC_value();
            variables.comand_dont_exist=0;
       break;
       case e_modificate_micro_time_on_led_green:
           if(variables.data_numeric<=pwm_green.LED_pwm_init_modificated)
           {
               pwm_green.LED_pwm_on_init_modificated=variables.data_numeric;
               initializate_pwm_green();
              print_times_for_pwm_led_green();
            }
           else
           {
               print_message_for_appropriate_uart("micro time should bigger than micro time on\n\r"); 
            }
            variables.comand_dont_exist=0;
       break;
       case e_modificate_micro_time_led_green:
            if(variables.data_numeric>=pwm_green.LED_pwm_on_init_modificated)
           {   
                if(variables.data_numeric<=pwm_green.pwm_on_init_modificated)
                { 
                   pwm_green.LED_pwm_init_modificated=variables.data_numeric;
                   initializate_pwm_green();
                   print_times_for_pwm_led_green();
                }
                else
               {
                  print_message_for_appropriate_uart("macro time on should bigger than micro time \n\r"); 
                }
            }   
           else
            {
              print_message_for_appropriate_uart("micro time should bigger than micro time on\n\r"); 
            }    
            variables.comand_dont_exist=0;
       break;
       case e_modificate_macro_time_on_led_green:
            if(variables.data_numeric<=pwm_green.pwm_init_modificated)
            {
                if(variables.data_numeric>=pwm_green.LED_pwm_init_modificated)
                {  
                   pwm_green.pwm_on_init_modificated=variables.data_numeric;
                   initializate_pwm_green();
                   print_times_for_pwm_led_green();
                }  
               else
                {
                  print_message_for_appropriate_uart("macro time on should bigger than micro time \n\r");
                }  
            }
            else
            {
                 print_message_for_appropriate_uart("macro time should bigger than macro time on\n\r"); 
            }
            variables.comand_dont_exist=0;
       break;
       case e_modificate_macro_time_led_green:
            if(variables.data_numeric>=pwm_green.pwm_on_init_modificated)
           {  
                pwm_green.pwm_init_modificated=variables.data_numeric;
                initializate_pwm_green();
                print_times_for_pwm_led_green();
            }
            else
            {
                 print_message_for_appropriate_uart("macro time should bigger than macro time on\n\r"); 
            }
            variables.comand_dont_exist=0;
       break;
       case e_modificate_micro_time_on_led_blue:
            if(variables.data_numeric<=pwm_blue.LED_pwm_init_modificated)
            {
                pwm_blue.LED_pwm_on_init_modificated=variables.data_numeric;
                initializate_pwm_blue();
                print_times_for_pwm_led_blue();
            }
            else
            {
                 print_message_for_appropriate_uart("micro time should bigger than micro time on\n\r"); 
            }
            variables.comand_dont_exist=0;
       break;
     case e_modificate_micro_time_led_blue:
       if(variables.data_numeric>=pwm_blue.LED_pwm_on_init_modificated)
       {   
         if(variables.data_numeric<=pwm_blue.pwm_on_init_modificated)
         { 
           pwm_blue.LED_pwm_init_modificated=variables.data_numeric;
           initializate_pwm_blue();
           print_times_for_pwm_led_blue();
         }
         else
         {
            print_message_for_appropriate_uart("macro time on should bigger than micro time \n\r"); 
          }
        }
       else
       {
            print_message_for_appropriate_uart("micro time should bigger than micro time on\n\r"); 
        }
        variables.comand_dont_exist=0;
     break;
     case e_modificate_macro_time_on_led_blue:
        if(variables.data_numeric<=pwm_blue.pwm_init_modificated)
        {
          if(variables.data_numeric>=pwm_blue.LED_pwm_init_modificated)
          {  
            pwm_blue.pwm_on_init_modificated=variables.data_numeric;
            initializate_pwm_blue();
            print_times_for_pwm_led_blue();
          }  
          else
          {
            print_message_for_appropriate_uart("macro time on should bigger than micro time \n\r");
          }  
        }
       else
       {
           print_message_for_appropriate_uart("macro time should bigger than macro time on\n\r"); 
        }
       variables.comand_dont_exist=0;
     break;
     case e_modificate_macro_time_led_blue:
         if(variables.data_numeric>=pwm_blue.pwm_on_init_modificated)
        {  
            pwm_blue.pwm_init_modificated=variables.data_numeric;
            initializate_pwm_blue();
            print_times_for_pwm_led_blue();
        }
       else
       {
           print_message_for_appropriate_uart("macro time should bigger than macro time on\n\r "); 
        }
       variables.comand_dont_exist=0;
     break;
       case e_enable_led_green:
            switch(variables.data_numeric)
           { 
                case 1:     
                    LED_green_struct.LED_status=e_LED_ON;
                    initializate_pwm_green();
                    print_message_for_appropriate_uart("LED GREEN PWM STATUS: ON\n\r"); 
                    variables.comand_dont_exist=0;
               break;
                case 0:
                    LED_green_struct.LED_status=e_LED_OFF;
                    mPORTBSetBits(BIT_3);
                    initializate_pwm_green();
                     print_message_for_appropriate_uart("LED GREEN PWM STATUS: OFF\n\r"); 
                    variables.comand_dont_exist=0;
                break;
            }
       break;
     
       case e_enable_led_blue:
           switch(variables.data_numeric)
           {
              case 0:
                    LED_blue_struct.LED_status=e_LED_OFF;
                    mPORTBSetBits(BIT_2);
                    initializate_pwm_blue();
                     print_message_for_appropriate_uart("LED BLUE PWM STATUS: OFF\n\r");
                    variables.comand_dont_exist=0;
              break;
              case 1:
                    LED_blue_struct.LED_status=e_LED_ON;
                    initializate_pwm_blue();
                    print_message_for_appropriate_uart("LED BLUE PWM STATUS: ON\n\r");
                    variables.comand_dont_exist=0;
              break;
            }
       break;    
     default:
        print_message_for_appropriate_uart("COMAND DON'T EXIST\n\r");
     break;
    }
  }

//For verify_comand function:
/**
    * @brief    verify the comand received and set or clear the respective flag   
    */
void verify_comand(void)
 {
   if (!memcmp(&variables.comand,"GADC",4))
      variables.comand_determinated=e_get_ADC_value;
   if (!memcmp(variables.comand,"GENB",4))
      variables.comand_determinated=e_enable_led_green;
   if (!memcmp(variables.comand,"GMIO",4))
      variables.comand_determinated=e_modificate_micro_time_on_led_green;
   if (!memcmp(variables.comand,"GMIP",4))
      variables.comand_determinated=e_modificate_micro_time_led_green;
   if (!memcmp(variables.comand,"GMAO",4))
      variables.comand_determinated=e_modificate_macro_time_on_led_green;
   if (!memcmp(variables.comand,"GMAP",4))
      variables.comand_determinated=e_modificate_macro_time_led_green;
   if (!memcmp(variables.comand,"BENB",4))
     variables.comand_determinated=e_enable_led_blue;
   if (!memcmp(&variables.comand,"DILB",4))
      variables.comand_determinated=e_disable_led_blue;
   if (!memcmp(&variables.comand,"BMIO",4))
      variables.comand_determinated=e_modificate_micro_time_on_led_blue;
   if (!memcmp(&variables.comand,"BMIP",4))
      variables.comand_determinated=e_modificate_micro_time_led_blue;
   if (!memcmp(&variables.comand,"BMAO",4))
      variables.comand_determinated=e_modificate_macro_time_on_led_blue;
    if (!memcmp(&variables.comand,"BMAP",4))
      variables.comand_determinated=e_modificate_macro_time_led_blue;
  }

//For print_times_for_pwm_led_green function:
/**
    * @brief    print via UART all times corresponding to led green pwm   
    */
void print_times_for_pwm_led_green(void)
{
    
   uint8_t  UART1_TX_buffer[100];
   UART2_write_string(UART1_TX_buffer);
   switch(variables.chosen_board)
   {       
        case UART_TO_COMPUTER:
            sprintf(UART1_TX_buffer, "Led green: micro time on is %d\n\r",pwm_green.LED_pwm_on_init_modificated);
            UART2_write_string(UART1_TX_buffer);
            sprintf(UART1_TX_buffer, "Led green: micro time is %d\n\r",pwm_green.LED_pwm_init_modificated);
            UART2_write_string(UART1_TX_buffer);
            sprintf(UART1_TX_buffer, "Led green: macro time on is %d\n\r",pwm_green.pwm_on_init_modificated);
            UART2_write_string(UART1_TX_buffer);
            sprintf(UART1_TX_buffer, "Led green: macro time is %d\n\r",pwm_green.pwm_init_modificated);
            UART2_write_string(UART1_TX_buffer);
        break;    
         case UART_TO_BOARD:      
             UART1_write_string("message received\n\r");
//            sprintf(UART1_TX_buffer, "Led green: micro time on is %d\n\r",pwm_green.LED_pwm_on_init_modificated);
//            UART1_write_string(UART1_TX_buffer);
//            sprintf(UART1_TX_buffer, "Led green: micro time is %d\n\r",pwm_green.LED_pwm_init_modificated);
//            UART1_write_string(UART1_TX_buffer);
//            sprintf(UART1_TX_buffer, "Led green: macro time on is %d\n\r",pwm_green.pwm_on_init_modificated);
//            UART1_write_string(UART1_TX_buffer);
//            sprintf(UART1_TX_buffer, "Led green: macro time is %d\n\r",pwm_green.pwm_init_modificated);
//            UART1_write_string(UART1_TX_buffer);
        break;    
   }
}

//For print_times_for_pwm_led_blue function:
/**
    * @brief    print via UART all times corresponding to led blue pwm   
    */
void print_times_for_pwm_led_blue(void)
 {
   uint8_t  UART1_TX_buffer[100];
   switch(variables.chosen_board)
   {       
       case UART_TO_COMPUTER:
            sprintf(UART1_TX_buffer, "Led blue: micro time on is %d\n\r",pwm_blue.LED_pwm_on_init_modificated);
            UART2_write_string(UART1_TX_buffer);
            sprintf(UART1_TX_buffer, "Led blue: micro time is %d\n\r",pwm_blue.LED_pwm_init_modificated);
            UART2_write_string(UART1_TX_buffer);
            sprintf(UART1_TX_buffer, "Led blue: macro time on is %d\n\r",pwm_blue.pwm_on_init_modificated);
            UART2_write_string(UART1_TX_buffer);
            sprintf(UART1_TX_buffer, "Led blue: macro time is %d\n\r",pwm_blue.pwm_init_modificated);
            UART2_write_string(UART1_TX_buffer);     
       break;
       case UART_TO_BOARD:
           UART1_write_string("message received\n\r");
//            sprintf(UART1_TX_buffer, "Led blue: micro time on is %d\n",pwm_blue.LED_pwm_on_init_modificated);
//            UART1_write_string(UART1_TX_buffer);
//            sprintf(UART1_TX_buffer, "Led blue: micro time is %d\n",pwm_blue.LED_pwm_init_modificated);
//            UART1_write_string(UART1_TX_buffer);
//            sprintf(UART1_TX_buffer, "Led blue: macro time on is %d\n",pwm_blue.pwm_on_init_modificated);
//            UART1_write_string(UART1_TX_buffer);
//            sprintf(UART1_TX_buffer, "Led blue: macro time is %d\n\r",pwm_blue.pwm_init_modificated);
//            UART1_write_string(UART1_TX_buffer);
        break;
    }
 }

//For print_message_for_appropriate_uart function:
/**
    * @brief    print messages to the uart1 or uart2 according to variables.num_board indicator.
    * @Input    string to be print in uart1 or uart2.
    *    
    */
void print_message_for_appropriate_uart(uint8_t *string)
{
    switch(variables.chosen_board)
    {
        case UART_TO_COMPUTER:
            UART2_write_string(string);
        break;
        case UART_TO_BOARD:
            UART1_write_string(string);
        break;
    }        
            
}

//For parsing_to_message function:
/**
    * @brief    parsing the received message  
    */
void parsing_to_message(void)
{
    memset(variables.message,0,U1RxBufSize);
    if(UART1_is_unread())
    memcpy(variables.message,U1RxBuf, U1RxBufSize);
    if(UART2_is_unread())
    memcpy(variables.message,U2RxBuf, U1RxBufSize);
    uint8_t  UART1_TX_buffer[100];
    variables.comand[0]=0;
    variables.data_numeric=0;
    variables.chosen_board=0;
    sscanf (variables.message,"%hu %s %hu", &variables.chosen_board, variables.comand,&variables.data_numeric);
    sprintf(UART1_TX_buffer, "read:%s %u %u\n\r", variables.comand, variables.chosen_board, variables.data_numeric);
    UART2_write_string(UART1_TX_buffer);
}