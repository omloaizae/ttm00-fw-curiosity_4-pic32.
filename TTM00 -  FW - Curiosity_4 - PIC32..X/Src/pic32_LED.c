#include "pic32_LED.h"



led_struct_t LED1_struct;
led_struct_t LED2_struct;
led_struct_t LED3_struct;
led_struct_t LED_green_struct;
led_struct_t LED_blue_struct;
led_struct_t LED_red_struct;

//For LED_on function:
/**
    * @brief    turn on a determined led.
    * @param    <1> name of the led
    */

void LED_on(uint8_t LED){
    switch (LED){
        case e_LED_1:
            mPORTESetBits(LED1_struct.LED_pin);
            break;
        case e_LED_2:
            mPORTESetBits(LED2_struct.LED_pin);
            break;
        case e_LED_3:
            mPORTESetBits(LED3_struct.LED_pin );
            break;
        case e_LED_green:
            mPORTBClearBits(LED_green_struct.LED_pin);
            break;
        case e_LED_red:
            mPORTBClearBits(LED_red_struct.LED_pin ); 
            break;
        case e_LED_blue:
            mPORTBClearBits(LED_blue_struct.LED_pin ); 
            break;    
        default:
            break;
    }
}

//For LED_off function:
/**
    * @brief    turn off a determined led.
    * @param    <1> name of the led
    */

void LED_off(uint8_t LED){
    switch (LED) {
        case e_LED_1:
            mPORTEClearBits(LED1_struct.LED_pin);
            break;
        case e_LED_2:
            mPORTEClearBits(LED2_struct.LED_pin);
            break;
        case e_LED_3:
            mPORTEClearBits(LED3_struct.LED_pin );
            break;
        case e_LED_green:
            mPORTBSetBits(LED_green_struct.LED_pin );
            break;
        case e_LED_blue:
            mPORTBSetBits(LED_blue_struct.LED_pin );
            break;
        default:
            break;
    }
}

//For LED_toggle function:
/**
    * @brief    toggle a determined led.
    * @param    <1> name of the led
    */

void LED_toggle(uint8_t LED){
    switch (LED){
        case e_LED_1:
            mPORTEToggleBits(LED1_struct.LED_pin);
            break;
        case e_LED_2:
            mPORTEToggleBits(LED2_struct.LED_pin);
            break;
        case e_LED_3:
            mPORTEToggleBits(LED3_struct.LED_pin );
            break;
        case e_LED_green:
            mPORTBToggleBits(LED_green_struct.LED_pin );
            break;
        case e_LED_red:
            mPORTBToggleBits(LED_red_struct.LED_pin);
            break;    
        default:
            break;
    }
}
