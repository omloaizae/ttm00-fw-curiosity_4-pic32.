# 1.Project Brief:
	1.1 Project summary: Project use timers for generate different time of blinking in leds of the curiosity microchip board 
	PIC32MX470F512H.

# 2.HW description:
	2.1 Links to MCU�s datasheet:
	-PIC32MX470F512H datasheet:
	http://ww1.microchip.com/downloads/en/devicedoc/60001185g.pdf
	-Curiosity Board user guide:
	http://ww1.microchip.com/downloads/en/DeviceDoc/70005283B.pdf

# 3.Serial commands:
	3.1 Link to the commands source/header.
		source --- main.c:
		https://bitbucket.org/omloaizae/ttm00-fw-curiosity_1-2-3-pic32./src/develop/TTM00%20-%20%20FW%20-%20Curiosity_1-2-3%20-%20PIC32.X/Src/main.c
		header -- main.h:
		https://bitbucket.org/omloaizae/ttm00-fw-curiosity_1-2-3-pic32./src/develop/TTM00%20-%20%20FW%20-%20Curiosity_1-2-3%20-%20PIC32.X/Inc/main.h 
	3.2 Serial command file for serial terminal: 
	docklighthttps://docklight.de/downloads/

	3.3 Serial configuration:
		-Baud Rate  --- 115200
		-Parity --- None	
		-Parity Error Char. --- (ignore)
		-Data Bits ---- 8
		-Stop Bits --- 1
		-Send/Receive Com --- The one assigned by your PC.

# 4.Prerrequisites:
	4.1. SDK version: MPLAB V3.61

	4.2. IDE : MPLAB X IDE V3.61

	4.3. Compiler version : XC 32 V1.42

	4.4 Project configuration:
		Categories : Microchip Embedded 
		Projects : Standalone Project
		Family : All Families
		Device : PIC32MX470F512H
		Hardware Tools : Microchip starter kits
		Compiler : XC32 (v1.42)
		Encoding: ISO-8859-1

# 5. Versioning: 
	5.1. Current version of the fw/sw:
	V2.1.20190803
	5.2. Description of the last version changes: 
		-New routines were created. 
		-New comments were added to explain how the routines work.
        -Comments from previous features were deleted.
        -A description of the purpose of the code was made.
        -A description of the purpose of every function of the code was made.
		
# 6. Commands UART
	GADV : Get ADC value
	GLMO : Set Green LED Macro ON time
	GLMT : Set Green LED Macro time
	GLOT : Set Green LED Micro ON time
	GLTI : Set Green LED Micro time
	BLMO : Set Blue LED Macro ON  time
	BLMT : Set Blue LED Macro time
	BLOT : Set Blue LED Micro ON time
	BLTI : Set Blue LED Micro time
	ENLG/DILG : Enable/disable green LED blink
	ENLB/DILB : Enable/disable Blue LED blink
		


		
# 7. Authors  : 	
	7.1. Project staff : Oscar Mario Loaiza Echeverri
	7.2. Maintainer contact : 
		email: oscar.loaiza@titoma.com
		Bitbucket user : Oscar Loaiza

		